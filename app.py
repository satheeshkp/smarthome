import datetime
import json
import os

import requests
from flask import Flask, render_template, request, session
from flask import jsonify
from flaskext.mysql import MySQL

app = Flask(__name__)

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'smarthome'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return "Hello Boss!  <a href='/logout'>Logout</a>"


@app.route('/getdata', methods=['POST'])
def do_admin_login():
    content = request.get_json()
    select = content['select']
    date = content['date']

    query = ""
    if select == 1:
        query = '''SELECT id,command,approval,date,amount FROM smarthome.userdata where status=1;'''
    if select == 2:
        query = "SELECT * FROM smarthome.userdata where status=1 and approval=0;"
    if select == 3:
        query = "SELECT * FROM smarthome.userdata where status=1 and approval=1;"
    if select == 4:
        query = "SELECT * FROM smarthome.userdata where status=1 and date(date)='" + date + "';"

    cur = mysql.connect().cursor()
    cur.execute(query)
    r = [dict((cur.description[i][0], value)
              for i, value in enumerate(row)) for row in cur.fetchall()]
    return jsonify({'data': r})


def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()


@app.route('/userinfo', methods=['POST'])
def insert_user_data():
    # print(request)
    # command = request.form['command']
    # device_id = request.form['device_id']

    print(request.is_json)
    content = request.get_json()
    print(content)

    command = content['command']
    device_id = content['device_id']
    print(command)
    print(device_id)

    query = "INSERT INTO smarthome.userdata(date,command)VALUES(now(),'" + command + "');"

    print(query)

    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()

    query = "select * from smarthome.user_configs where id=1;"
    print(query)
    cursor.execute(query)
    while True:
        row = cursor.fetchone()
        if row == None:
            break
        url = 'https://fcm.googleapis.com/fcm/send'
        data = {'to': row[1], "notification": {
            "body": command,
            "title": "Order Received"
        }}
        headers = {'Content-type': 'application/json',
                   'Authorization': 'key=AAAA9t8BfLE:APA91bE7CO0MYZY9FS0cdkuF3_a1WJAQsNd-ktDpH3B7lNMaC-BqtBKdKDpnRGSLK18_MnhGKEF1wVNyCxW6zOl1qQGZYO8goqXJ78dcRtMJvKaGezTi9cSkfzQIc9gWAHkFwWmZ4Wu5H-yuEzPmSPKE1Lvm3A9wcw'}
        r = requests.post(url, data=json.dumps(data), headers=headers)

    return "1"


@app.route('/fcmTokenUpdate', methods=['POST'])
def fcm_token_update():
    # print(request)
    # command = request.form['command']
    # device_id = request.form['device_id']

    print(request.is_json)
    content = request.get_json()
    print(content)

    fcm_token = content['fcm_token']
    device_id = content['device_id']
    print(device_id)

    query = "UPDATE smarthome.user_configs SET fcm_token = '" + fcm_token + "' WHERE id = " + device_id + ";"

    print(query)

    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()

    return "1"


@app.route('/approve', methods=['POST'])
def approve_order():
    print(request.is_json)
    content = request.get_json()
    print(content)

    id = content['id']
    amount = content['amount']

    query = "UPDATE smarthome.userdata SET amount = '" + amount + "',approval ='1' WHERE id = " + id + ";"

    print(query)

    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()

    data = {'approve': 1}
    data = json.dumps(data)
    print(data)
    return data


@app.route('/login', methods=['POST'])
def admin_login():
    print(request.is_json)
    content = request.get_json()
    print(content)

    username = content['username']
    password = content['password']
    conn = mysql.connect()
    cursor = conn.cursor()
    query="SELECT * FROM smarthome.user_configs where username='" + username + "' and password='" + password + "' and status=1"
    cursor.execute(query)
    results = cursor.fetchone()
    print(results)
    success=0

    if results==None:
        success=0
    else:
        success=1
    # if len(results) > 1:
    #     success=1

    data = {'approve': success}
    data = json.dumps(data)
    print(data)
    return data


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True, host='0.0.0.0', port=4000)
